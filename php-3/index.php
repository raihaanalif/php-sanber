<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Name: $sheep->name <br>"; 
echo "Legs: $sheep->legs <br>"; 
echo "cold blooded: $sheep->cold_blooded <br>";

echo "<br>";
$buduk = new Frog("buduk");

echo "Name: $buduk->name <br>";
echo "Legs: $buduk->legs <br>";
echo "cold blooded: $buduk->cold_blooded <br>";
echo "Jump: ";
$buduk->jump();

echo "<br><br>";
$sungokong = new Ape("kera sakti");

echo "Name: $sungokong->name <br>";
echo "Legs: $sungokong->legs <br>";
echo "cold blooded: $sungokong->cold_blooded <br>";
echo "Yell: ";
$sungokong->yell();

