<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function form(){
        return view ('registration');
    }

    public function submit(Request $request){
        // dd($request->all());

        $fname = $request->fname;
        $lname = $request->lname;
        $fullname = $fname." ".$lname;

        return view ('welcome', ['name' => $fullname]);

    }
}
