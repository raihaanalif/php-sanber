<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/submit" method="POST">
        @csrf
        <label for="fname">First Name:</label><br><br>
        <input type="text" name="fname" id="fname" required><br><br>
        <label for="lname">Last Name:</label><br><br>
        <input type="text" name="lname" id="lname" required><br><br>

        <p>Gender:</p>
        <input type="radio" name="gender" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality:</label>
        <br><br>
        <select name="nationality" id="nationality" required>
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" name="language" id="bahasa">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" id="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" id="other">
        <label for="other">Other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" rows="10" cols="30" required></textarea><br>

        <input type="submit" value="masuk">
    </form>

</body>
</html>
